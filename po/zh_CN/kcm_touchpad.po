msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-25 00:40+0000\n"
"PO-Revision-Date: 2024-04-22 15:58\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-desktop/kcm_touchpad.pot\n"
"X-Crowdin-File-ID: 43419\n"

#: actions.cpp:19
#, kde-format
msgid "Touchpad"
msgstr "触摸板"

#: actions.cpp:22
#, kde-format
msgid "Enable Touchpad"
msgstr "启用触摸板"

#: actions.cpp:30
#, kde-format
msgid "Disable Touchpad"
msgstr "禁用触摸板"

#: actions.cpp:38
#, kde-format
msgid "Toggle Touchpad"
msgstr "切换触摸板开关"

#: backends/kwin_wayland/kwinwaylandbackend.cpp:59
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr "查询输入设备失败。请重新打开此设置模块。"

#: backends/kwin_wayland/kwinwaylandbackend.cpp:74
#, kde-format
msgid "Critical error on reading fundamental device infos for touchpad %1."
msgstr "读取触摸板 %1 的基本设备信息时发生严重错误。"

#: backends/x11/xlibbackend.cpp:71
#, kde-format
msgid "Cannot connect to X server"
msgstr "无法连接到 X 服务器"

#: backends/x11/xlibbackend.cpp:84 kcm/libinput/touchpad.qml:98
#, kde-format
msgid "No touchpad found"
msgstr "没有找到触摸板"

#: backends/x11/xlibbackend.cpp:124 backends/x11/xlibbackend.cpp:138
#, kde-format
msgid "Cannot apply touchpad configuration"
msgstr "无法应用触摸板配置"

#: backends/x11/xlibbackend.cpp:152 backends/x11/xlibbackend.cpp:165
#, kde-format
msgid "Cannot read touchpad configuration"
msgstr "无法读取触摸板配置"

#: backends/x11/xlibbackend.cpp:178
#, kde-format
msgid "Cannot read default touchpad configuration"
msgstr "无法读取默认触摸板配置"

#: kcm/libinput/touchpad.qml:99
#, kde-format
msgid "Connect an external touchpad"
msgstr "连接外置触摸板"

#: kcm/libinput/touchpad.qml:113
#, kde-format
msgid "Device:"
msgstr "设备："

#: kcm/libinput/touchpad.qml:139
#, kde-format
msgid "General:"
msgstr "常规："

#: kcm/libinput/touchpad.qml:140
#, kde-format
msgid "Device enabled"
msgstr "启用触摸板设备"

#: kcm/libinput/touchpad.qml:144
#, kde-format
msgid "Accept input through this device."
msgstr "接受此触摸板设备的输入。"

#: kcm/libinput/touchpad.qml:168
#, kde-format
msgid "Disable while typing"
msgstr "键盘输入时禁用触摸板"

#: kcm/libinput/touchpad.qml:172
#, kde-format
msgid "Disable touchpad while typing to prevent accidental inputs."
msgstr "使用键盘输入时禁用触摸板，以避免误触。"

#: kcm/libinput/touchpad.qml:196
#, kde-format
msgid "Left handed mode"
msgstr "左手模式"

#: kcm/libinput/touchpad.qml:200
#, kde-format
msgid "Swap left and right buttons."
msgstr "将左键和右键对调。"

#: kcm/libinput/touchpad.qml:226
#, kde-format
msgid "Press left and right buttons for middle click"
msgstr "同时按下左键和右键可触发中键点击"

#: kcm/libinput/touchpad.qml:230 kcm/libinput/touchpad.qml:878
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr "同时点击左键和右键来触发中键点击。"

#: kcm/libinput/touchpad.qml:256
#, kde-format
msgid ""
"Activating this setting increases mouse click latency by 50ms. The extra "
"delay is needed to correctly detect simultaneous left and right mouse clicks."
msgstr ""
"激活此设置将增加鼠标点击延迟 50 毫秒。正确检测鼠标左键和右键的同时点击需要额"
"外的延迟。"

#: kcm/libinput/touchpad.qml:266
#, kde-format
msgid "Pointer speed:"
msgstr "光标速度："

#: kcm/libinput/touchpad.qml:364
#, kde-format
msgid "Pointer acceleration:"
msgstr "光标加速："

#: kcm/libinput/touchpad.qml:395
#, kde-format
msgid "None"
msgstr "无"

#: kcm/libinput/touchpad.qml:399
#, kde-format
msgid "Cursor moves the same distance as finger."
msgstr "光标移动的距离与手指移动的距离相同。"

#: kcm/libinput/touchpad.qml:408
#, kde-format
msgid "Standard"
msgstr "标准"

#: kcm/libinput/touchpad.qml:412
#, kde-format
msgid "Cursor travel distance depends on movement speed of finger."
msgstr "光标移动的距离取决于手指移动的速度。"

#: kcm/libinput/touchpad.qml:427
#, kde-format
msgid "Tapping:"
msgstr "单指轻触："

#: kcm/libinput/touchpad.qml:428
#, kde-format
msgid "Tap-to-click"
msgstr "轻触点击"

#: kcm/libinput/touchpad.qml:432
#, kde-format
msgid "Single tap is left button click."
msgstr "单指轻触时触发左键点击。"

#: kcm/libinput/touchpad.qml:461
#, kde-format
msgid "Tap-and-drag"
msgstr "轻触拖拽"

#: kcm/libinput/touchpad.qml:465
#, kde-format
msgid "Sliding over touchpad directly after tap drags."
msgstr "单指轻触后在触摸板上滑动之间触发拖拽。"

#: kcm/libinput/touchpad.qml:492
#, kde-format
msgid "Tap-and-drag lock"
msgstr "轻触拖拽锁定"

#: kcm/libinput/touchpad.qml:496
#, kde-format
msgid "Dragging continues after a short finger lift."
msgstr "指尖短暂松开时继续拖拽。"

#: kcm/libinput/touchpad.qml:516
#, kde-format
msgid "Two-finger tap:"
msgstr "双指轻触："

#: kcm/libinput/touchpad.qml:527
#, kde-format
msgid "Right-click (three-finger tap to middle-click)"
msgstr "右键点击 (三指轻触为中键点击)"

#: kcm/libinput/touchpad.qml:528
#, kde-format
msgid ""
"Tap with two fingers to right-click, tap with three fingers to middle-click."
msgstr "双指轻触触发右键点击，三指轻触触发中键点击。"

#: kcm/libinput/touchpad.qml:530
#, kde-format
msgid "Middle-click (three-finger tap right-click)"
msgstr "中键点击 (三指轻触为右键点击)"

#: kcm/libinput/touchpad.qml:531
#, kde-format
msgid ""
"Tap with two fingers to middle-click, tap with three fingers to right-click."
msgstr "双指轻触触发中键点击，三指轻触触发右键点击。"

#: kcm/libinput/touchpad.qml:533
#, kde-format
msgid "Right-click"
msgstr "右键点击"

#: kcm/libinput/touchpad.qml:534
#, kde-format
msgid "Tap with two fingers to right-click."
msgstr "双指轻触为右键点击。"

#: kcm/libinput/touchpad.qml:536
#, kde-format
msgid "Middle-click"
msgstr "中键点击"

#: kcm/libinput/touchpad.qml:537
#, kde-format
msgid "Tap with two fingers to middle-click."
msgstr "双指轻触为中键点击。"

#: kcm/libinput/touchpad.qml:596
#, kde-format
msgid "Scrolling:"
msgstr "滚动："

#: kcm/libinput/touchpad.qml:625
#, kde-format
msgid "Two fingers"
msgstr "双指滚动"

#: kcm/libinput/touchpad.qml:629
#, kde-format
msgid "Slide with two fingers scrolls."
msgstr "双指滑动时触发滚动。"

#: kcm/libinput/touchpad.qml:637
#, kde-format
msgid "Touchpad edges"
msgstr "触摸板边缘滚动"

#: kcm/libinput/touchpad.qml:641
#, kde-format
msgid "Slide on the touchpad edges scrolls."
msgstr "手指在触摸板边缘滑动时滚动。"

#: kcm/libinput/touchpad.qml:651
#, kde-format
msgid "Invert scroll direction (Natural scrolling)"
msgstr "反向滚动 (自然滚动)"

#: kcm/libinput/touchpad.qml:667
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "模仿触摸屏的滚动方式。"

#: kcm/libinput/touchpad.qml:675 kcm/libinput/touchpad.qml:692
#, kde-format
msgid "Disable horizontal scrolling"
msgstr "禁用水平滚动"

#: kcm/libinput/touchpad.qml:700
#, kde-format
msgid "Scrolling speed:"
msgstr "滚动速度："

#: kcm/libinput/touchpad.qml:751
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "慢"

#: kcm/libinput/touchpad.qml:758
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "快"

#: kcm/libinput/touchpad.qml:769
#, kde-format
msgid "Right-click:"
msgstr "右键点击："

#: kcm/libinput/touchpad.qml:803
#, kde-format
msgid "Press bottom-right corner"
msgstr "按下触摸板的右下角"

#: kcm/libinput/touchpad.qml:807
#, kde-format
msgid ""
"Software enabled buttons will be added to bottom portion of your touchpad."
msgstr "触摸板的底部将增加虚拟按键。"

#: kcm/libinput/touchpad.qml:815
#, kde-format
msgid "Press anywhere with two fingers"
msgstr "双指按下触摸板的任意位置"

#: kcm/libinput/touchpad.qml:819
#, kde-format
msgid "Tap with two finger to enable right click."
msgstr "双指轻触为右键点击。"

#: kcm/libinput/touchpad.qml:833
#, kde-format
msgid "Middle-click: "
msgstr "中键点击："

#: kcm/libinput/touchpad.qml:862
#, kde-format
msgid "Press bottom-middle"
msgstr "按下触摸板的底边中间"

#: kcm/libinput/touchpad.qml:866
#, kde-format
msgid ""
"Software enabled middle-button will be added to bottom portion of your "
"touchpad."
msgstr "触摸板的底部将增加中键的虚拟按键。"

#: kcm/libinput/touchpad.qml:874
#, kde-format
msgid "Press bottom left and bottom right corners simultaneously"
msgstr "同时按下触摸板的左下角和右下角"

#: kcm/libinput/touchpad.qml:887
#, kde-format
msgid "Press anywhere with three fingers"
msgstr "三指按下触摸板的任意位置"

#: kcm/libinput/touchpad.qml:893
#, kde-format
msgid "Press anywhere with three fingers."
msgstr "三指按下触摸板的任意位置。"

#: kcm/touchpadconfig.cpp:99
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr "加载数值时发生错误。请查看日志以获取更多信息。请重启此设置模块。"

#: kcm/touchpadconfig.cpp:102
#, kde-format
msgid "No touchpad found. Connect touchpad now."
msgstr "没有找到触摸板。请连接触摸板。"

#: kcm/touchpadconfig.cpp:111
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr "无法保存全部更改。请查看日志以获取更多信息。请重启此设置模块并重试。"

#: kcm/touchpadconfig.cpp:130
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr "加载默认值时出错。无法将某些选项重置为默认值。"

#: kcm/touchpadconfig.cpp:150
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr "添加新连接的设备时出错。请重新连接，并重启此设置模块。"

#: kcm/touchpadconfig.cpp:173
#, kde-format
msgid "Touchpad disconnected. Closed its setting dialog."
msgstr "触摸板已断开连接。已关闭它的设置对话框。"

#: kcm/touchpadconfig.cpp:175
#, kde-format
msgid "Touchpad disconnected. No other touchpads found."
msgstr "触摸板已断开连接。没有找到其他触摸板。"

#: kded/kded.cpp:201
#, kde-format
msgid "Touchpad was disabled because a mouse was plugged in"
msgstr "检测到鼠标插入，触摸板已禁用"

#: kded/kded.cpp:204
#, kde-format
msgid "Touchpad was enabled because the mouse was unplugged"
msgstr "检测到鼠标拔出，触摸板已启用"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:27
#, kde-format
msgctxt "Emulated mouse button"
msgid "No action"
msgstr "无操作"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:30
#, kde-format
msgctxt "Emulated mouse button"
msgid "Left button"
msgstr "左键"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:33
#, kde-format
msgctxt "Emulated mouse button"
msgid "Middle button"
msgstr "中键"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:36
#, kde-format
msgctxt "Emulated mouse button"
msgid "Right button"
msgstr "右键"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:285
#, kde-format
msgctxt "Touchpad Edge"
msgid "All edges"
msgstr "所有边缘"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:288
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top edge"
msgstr "顶部边缘"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:291
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top right corner"
msgstr "右上角"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:294
#, kde-format
msgctxt "Touchpad Edge"
msgid "Right edge"
msgstr "右边缘"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:297
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom right corner"
msgstr "右下角"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:300
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom edge"
msgstr "底部边缘"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:303
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom left corner"
msgstr "左下角"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:306
#, kde-format
msgctxt "Touchpad Edge"
msgid "Left edge"
msgstr "左边缘"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:309
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top left corner"
msgstr "左上角"
