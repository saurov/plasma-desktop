# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Tommi Nieminen <translator@legisign.org>, 2022, 2023, 2024.
# SPDX-FileCopyrightText: 2022, 2024 Lasse Liehu <lasse.liehu@iki.fi>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-10 00:39+0000\n"
"PO-Revision-Date: 2024-02-29 20:02+0200\n"
"Last-Translator: Lasse Liehu <lasse.liehu@iki.fi>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.5\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Ensisijainen (oletus)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Pysty"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Vaaka"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Käänteinen pysty"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Käänteinen vaaka"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Seuraa aktiivista näyttöä"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 – (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "Sovita ulostuloon"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Sovita ulostulo piirtopöytään"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Mukautettu koko"

#: ui/main.qml:28
#, kde-format
msgid "No drawing tablets found"
msgstr "Piirtopöytiä ei löytynyt"

#: ui/main.qml:29
#, kde-format
msgid "Connect a drawing tablet"
msgstr "Yhdistä piirtopöytään"

#: ui/main.qml:41
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Laite:"

#: ui/main.qml:79
#, kde-format
msgid "Target display:"
msgstr "Kohdenäyttö:"

#: ui/main.qml:98
#, kde-format
msgid "Orientation:"
msgstr "Suunta:"

#: ui/main.qml:110
#, fuzzy, kde-format
#| msgid "Left-handed mode:"
msgid "Left handed mode:"
msgstr "Vasenkätinen tila:"

#: ui/main.qml:120
#, kde-format
msgid "Area:"
msgstr "Ala:"

#: ui/main.qml:208
#, kde-format
msgid "Resize the tablet area"
msgstr "Muuta piirtopöydän kokoa"

#: ui/main.qml:232
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Lukitse kuvasuhde"

#: ui/main.qml:240
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 – %3×%4"

#: ui/main.qml:249
#, fuzzy, kde-format
#| msgid "Pen Button 1"
msgid "Pen button 1:"
msgstr "Kynäpainike 1"

#: ui/main.qml:250
#, fuzzy, kde-format
#| msgid "Pen Button 2"
msgid "Pen button 2:"
msgstr "Kynäpainike 2"

#: ui/main.qml:251
#, fuzzy, kde-format
#| msgid "Pen Button 3"
msgid "Pen button 3:"
msgstr "Kynäpainike 3"

#: ui/main.qml:295
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Laite:"

#: ui/main.qml:306
#, kde-format
msgid "None"
msgstr "Ei mitään"

#: ui/main.qml:328
#, fuzzy, kde-format
#| msgid "Button %1:"
msgid "Pad button %1:"
msgstr "Painike %1:"

#~ msgid "Tool Button 1"
#~ msgstr "Työkalupainike 1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Tommi Nieminen"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "translator@legisign.org"

#~ msgid "Tablet"
#~ msgstr "Piirtopöytä"
